#!/bin/bash
TESTUSER=${TESTUSER:-test}
echo "Creating $TESTUSER ..."
EVANS=""
# account proto package
accountPackage="actourex.core.service.account.proto"
# user creation data
createData='{"meta": {"requestId": "testrequest"}, "account": {"id": "", "orgCode": "TST", "username": '
createData=$createData\"$TESTUSER\"
createData=$createData', "password": "test", "givenName": "test", "surname": "tester", "email": '
createData=$createData'"test@redeam.com", "phone": "1234567890"}}'
# user creation misc
createFunction="CreateAccount"
createService="Command"
createPackage=$accountPackage
# list groups data
listData='{"meta": {"requestId": "foobarz"}, "criteria": {"limit": 0, "offset": 0}}'
# list groups misc
listFunction="ListGroups"
listService="Query"
listPackage=$accountPackage
listGroup="REDEAM_TECHNICAL_STAFF_TEST"
# group creation data
# group creation misc
groupFunction="AddAccountToGroup"
groupService="Command"
groupPackage=$accountPackage
# ping data
pingData='{"theAnswer": "100"}'
pingFunction="Ping"
pingService="Query"
pingPackage=$accountPackage
# proto file
protoFile="./services/account/proto/service.proto"
# invoke evans
cd $CORE
allPorts=`netstat -ant | awk '{print $4}' | sed 's/.*://' | tail -n +3`
for port in $allPorts; do
	echo "Looking for account-svc on $port ..."
	EVANS="evans --host 127.0.0.1 --port $port "
	check=`echo $pingData | $EVANS \
			--package $pingPackage \
			--service $pingService \
			--call $pingFunction $protoFile`
	if [[ $check = *theAnswer* ]]; then
		echo "Found account-svc on $port!"
		break
	fi
done
# create initial user
user=`echo $createData | $EVANS \
			--package $createPackage \
			--service $createService \
			--call $createFunction $protoFile`
userId=`echo $user | jq '.account | .id'`
# list back groups and parse out one with jq
groups=`echo $listData | $EVANS \
			--package $listPackage \
			--service $listService \
			--call $listFunction $protoFile`
# fetch a group id to assign our new user to
groupId=`echo $groups | jq ".groups[] | .[] | select ( .name == \"$listGroup\") |  .id"`
# foo
groupData='{"meta": {"requestId": "foobarz"}, "account_id": '
groupData=$groupData"$userId, "
groupData=$groupData'"group_id": '
groupData=$groupData"$groupId}}"
# make the bloody group
group=`echo $groupData | $EVANS \
			--package $groupPackage \
			--service $groupService \
			--call $groupFunction $protoFile`
echo "Succes! $TESTUSER created and assigned to $listGroup."
