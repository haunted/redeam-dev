# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

# system
VAGRANT_BOX = ENV['RDEV_BOX'] || 'minimal/trusty64'
VM_NAME = ENV['RDEV_VM'] || 'redeam-dev'
HOST_USER = ENV['USER']
HYPERVISOR = ENV['RDEV_PROVIDER'] || 'virtualbox'
APT_PACKAGES = 'git curl unzip ca-certificates bash openssh-client python-openssl' + 
                ' socat jq rsync tar apt-transport-https software-properties-common' +
                ' poppler-utils npm lsof postgresql-client' 

# go
GO_VERSION = ENV['RDEV_GO_VERSION'] || '1.10.2'
GO_FILE = 'go' + GO_VERSION + '.linux-amd64.tar.gz'
GO_URI = 'https://dl.google.com/go'
GO_URL = GO_URI + '/' + GO_FILE
GO_INSTALL_DIR = ENV['RDEV_GO_INSTALL_DIR'] || '/usr/local'
GOPATH = ENV['RDEV_GOPATH'] || '/home/vagrant/go'

# google cloud sdk
GCSDK_FILE = 'google-cloud-sdk.zip'
GCSDK_URI = 'https://dl.google.com/dl/cloudsdk/channels/rapid'
GCSDK_URL = GCSDK_URI + '/' + GCSDK_FILE
GCSDK_INSTALL_SWITCHES = '--usage-reporting=true --path-update=true' +
                            ' --bash-completion=true' +
                            ' --rc-path=$HOME/.bashrc' +
                            ' --additional-components'

GCSDK_ADDITIONAL = 'alpha app-engine-go app-engine-java app-engine-python' +
                       ' beta bigtable bq cloud-datastore-emulator' +
                       ' docker-credential-gcr gcd-emulator gsutil' +
                       ' kubectl pubsub-emulator'
GCSDK_CONFIG_VALUES = '--installation component_manager/disable_update_check true'

# docker-compose
DOCKER_COMPOSE_VERSION = ENV['RDEV_DOCKER_COMPOSE_VERSION'] || '1.21.2'
DOCKER_COMPOSE_FILE = 'docker-compose-Linux-x86_64'
DOCKER_COMPOSE_URI = 'https://github.com/docker/compose/releases/download/' +
                        DOCKER_COMPOSE_VERSION 
DOCKER_COMPOSE_URL = DOCKER_COMPOSE_URI + '/' + DOCKER_COMPOSE_FILE 

# jet
JET_VERSION = ENV['RDEV_JET_VERSION'] || '2.6.0'
JET_FILE = 'jet-linux_amd64_' + JET_VERSION + '.tar.gz'
JET_URI = 'https://s3.amazonaws.com/codeship-jet-releases/' + JET_VERSION
JET_URL = JET_URI + '/' + JET_FILE

# protoc
PROTO_VERSION = ENV['RDEV_PROTO_VERSION'] || '3.5.1'
PROTO_FILE = 'protoc-' + PROTO_VERSION + '-linux-x86_64.zip'
PROTO_URI = 'https://github.com/google/protobuf/releases/download/v'
PROTO_URL = PROTO_URI + PROTO_VERSION + '/' + PROTO_FILE

# redeam
REDEAM_PATH = ENV['RDEV_REDEAM_PATH'] || '/home/vagrant/go/src/gitlab.com/redeam'
CORE_PATH = ENV['RDEV_CORE_PATH'] || REDEAM_PATH + '/core'
CORE_REPO_URL = ENV['RDEV_CORE_REPO_URL'] || 'https://gitlab.com/redeam/core.git'

# redeam tests
POSTGRES_SSLMODE = ENV['RDEV_POSTGRES_SSLMODE'] || 'disable'
POSTGRES_HOST = ENV['RDEV_POSTGRES_HOST'] || 'postgres'
POSTGRES_DB = ENV['RDEV_POSTGRES_DB'] || 'actourex'
POSTGRES_USER = ENV['RDEV_POSTGRES_USER'] || 'actourex'
POSTGRES_MIGRATIONS = ENV['RDEV_POSTGRES_MIGRATIONS'] || '/pkg/account/backend/postgres/migrations'
ACCOUNT_LENGTH_TOKEN = ENV['RDEV_ACCOUNT_LENGTH_TOKEN'] || 12
ACCOUNT_ACCESS_TOKEN_TTL = ENV['RDEV_ACCOUNT_ACCESS_TOKEN_TTL'] || '10m'
ACCOUNT_REFRESH_TOKEN_TTL = ENV['RDEV_ACCOUNT_REFRESH_TOKEN_TTL'] || '60m'
INFLUXDB_ADDR = ENV['RDEV_INFLUXDB_ADDR'] || 'http://influxdb:60086'
TEST_IN_LOCAL = ENV['RDEV_TEST_IN_LOCAL'] || true
GCS_CREDS_FILE = ENV['RDEV_GCS_CREDS_FILE'] || '$CORE_PATH/.google-account.json'
GCS_VOUCHER_BUCKET = ENV['RDEV_GCS_VOUCHER_BUCKET'] || 'training_vouchers'
GSHEET_CONFIG_FILE = ENV['RDEV_GSHEET_CONFIG_FILE'] || '$CORE_PATH/.google-sheets.json'
CF_VOUCHER_EXPIRES_IN = ENV['RDEV_CF_VOUCHER_EXPIRES_IN'] || '120h'
CF_USERNAME = ENV['RDEV_CF_USERNAME'] || '712483d5af8047d0b364bc61076d35f7'
CF_PASSWORD = ENV['RDEV_CF_PASSWORD'] || 'ed02fe1aab3a4daa9f0596d77c74b627'
CF_LINE_ID = ENV['RDEV_CF_LINE_ID'] || '6N3Rvbg1FW'
CF_CALLBACK_URL = ENV['RDEV_CF_CALLBACK_URL'] || 'https://cf-develop.actourex.io/v1/callback'
CF_GSHEET_ID = ENV['RDEV_CF_GSHEET_ID'] || '1UMPQMg6dGanGmiNpkK3MzBDkwE6QCPwUC_AGNOKDIWM'
SLACK_TOKEN = ENV['RDEV_SLACK_TOKEN'] || 'foobar'
PASSHUB_BASE_URL = ENV['RDEV_PASSHUB_BASE_URL'] || 'https://app.sightseeingpass.com'
PASSHUB_USERNAME = ENV['RDEV_PASSHUB_USERNAME'] || 'actourex1admin'
PASSHUB_PASSWORD = ENV['RDEV_PASSHUB_PASSWORD'] || 'wehatepaper'
PASSHUB_VENDOR_ID = ENV['RDEV_PASSHUB_VENDOR_ID'] || 77221

# shared folders
HOST_PATH = ENV['RDEV_LOCAL_CORE_PATH'] || '/Users/' + HOST_USER + '/core'

Vagrant.configure(2) do |config|
  config.vm.box = VAGRANT_BOX
  config.vm.hostname = VM_NAME
  config.vm.provider HYPERVISOR do |v|
    v.name = VM_NAME
    v.memory = 2048
    v.customize ["modifyvm", :id, "--usb", "on"]
    v.customize ["modifyvm", :id, "--usbehci", "off"]
  end
  # sync repo
  config.vm.synced_folder HOST_PATH, CORE_PATH, type: 'rsync',
    rsync__args: ['--verbose', '--archive', '-z'],
    rsync__exclude: ['.*.swp', 'vendor/']
  # go ahead anmd sync the redeam-dev content
  config.vm.synced_folder '.', '/home/vagrant', disabled: true
  
  # make it so
  config.vm.provision 'shell',
    privileged: false, 
    env: {
        'REDEAM_PATH' => REDEAM_PATH,
        'CORE_PATH' => CORE_PATH,
        'CORE' => CORE_PATH,
        'CR_PTH' => CORE_PATH,
        'CORE_REPO_URL' => CORE_REPO_URL,
        'GO_INSTALL_DIR' => GO_INSTALL_DIR,
        'GO_VERSION' => GO_VERSION,
        'GO_URL' => GO_URL,
        'GO_FILE' => GO_FILE,
        'GOPATH' => GOPATH,
        'JET_URL' => JET_URL,
        'JET_FILE' => JET_FILE,
        'JET_VERSION' => JET_VERSION,
        'PROTO_URL' => PROTO_URL,
        'PROTO_FILE' => PROTO_FILE,
        'PROTO_VERSION' => PROTO_VERSION,
        'GCSDK_URL' => GCSDK_URL,
        'GCSDK_FILE' => GCSDK_FILE,
        'GCSDK_ADDITIONAL' => GCSDK_ADDITIONAL,
        'GCSDK_INSTALL_SWITCHES' => GCSDK_INSTALL_SWITCHES,
        'GCSDK_CONFIG_VALUES' => GCSDK_CONFIG_VALUES,
        'APT_PACKAGES' => APT_PACKAGES,
        'DOCKER_COMPOSE_URL' => DOCKER_COMPOSE_URL,
        'POSTGRES_SSLMODE' => POSTGRES_SSLMODE,
        'POSTGRES_DB' => POSTGRES_DB,
        'POSTGRES_HOST' => POSTGRES_HOST,
        'POSTGRES_USER' => POSTGRES_USER,
        'POSTGRES_MIGRATIONS' => POSTGRES_MIGRATIONS,
        'ACCOUNT_ACCESS_TOKEN_TTL' => ACCOUNT_ACCESS_TOKEN_TTL,
        'ACCOUNT_LENGTH_TOKEN' => ACCOUNT_LENGTH_TOKEN,
        'ACCOUNT_REFRESH_TOKEN_TTL' => ACCOUNT_REFRESH_TOKEN_TTL,
        'INFLUXDB_ADDR' => INFLUXDB_ADDR,
        'TEST_IN_LOCAL' => TEST_IN_LOCAL,
        'GCS_CREDS_FILE' => GCS_CREDS_FILE,
        'GCS_VOUCHER_BUCKET' => GCS_VOUCHER_BUCKET,
        'GSHEET_CONFIG_FILE' => GSHEET_CONFIG_FILE,
        'CF_VOUCHER_EXPIRES_IN' => CF_VOUCHER_EXPIRES_IN,
        'CF_USERNAME' => CF_USERNAME,
        'CF_PASSWORD' => CF_PASSWORD,
        'CF_LINE_ID' => CF_LINE_ID,
        'CF_CALLBACK_URL' => CF_CALLBACK_URL,
        'CF_GSHEET_ID' => CF_GSHEET_ID,
        'SLACK_TOKEN' => SLACK_TOKEN,
        'PASSHUB_VENDOR_ID' => PASSHUB_VENDOR_ID,
        'PASSHUB_BASE_URL' => PASSHUB_BASE_URL,
        'PASSHUB_PASSWORD' => PASSHUB_PASSWORD,
        'PASSHUB_USERNAME' => PASSHUB_USERNAME,
        'CORE_DOCKER_IP' => 'localhost',
        'TERM' => 'xterm-256color'
    },
    inline: <<-SHELL
        set -x
        PATH=$PATH:$GOPATH/bin:/usr/local/go/bin:$HOME/google-cloud-sdk/bin
        sudo chown -R $USER.$USER $HOME/*
        ### apt-get required packages
        sudo apt-get -qq update
        sudo apt-get -qq install -y $APT_PACKAGES
        ### vim packages - we want vim 8
        sudo add-apt-repository ppa:jonathonf/vim
        sudo apt-get -qq update
        sudo apt-get -qq install -y vim
        ### install Plug for VIM
        curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        ### populate vimrc 
        curl -fLo $HOME/.vimrc --create-dirs https://raw.githubusercontent.com/fatih/vim-go-tutorial/master/vimrc
        ### install go
        if [ ! -d "$GO_INSTALL_DIR/go" ]
            then
                cd /tmp
                curl -SsLO $GO_URL
                tar -xf $GO_FILE
                sudo mv go $GO_INSTALL_DIR
                rm $GO_FILE
        fi
        ### install docker
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable"
        sudo apt-get -qq update
        sudo apt-get -qq install -y docker-ce
        sudo usermod -a -G docker $USER
        ### install boilr
        cd $GOPATH
        go get -u github.com/tmrts/boilr
        boilr init
        ### install goimports (linting)
        go get -u golang.org/x/tools/cmd/goimports
        ### install docker-compose
        sudo curl -SsL $DOCKER_COMPOSE_URL -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
        ### install google cloud sdk
        if [ ! -d "$HOME/google-cloud-sdk" ]
            then
                cd $HOME
                curl -SsLO $GCSDK_URL
                unzip -qq $GCSDK_FILE
                rm $GCSDK_FILE
                google-cloud-sdk/install.sh $GCSDK_INSTALL_SWITCHES $GCSDK_ADDITIONAL
                google-cloud-sdk/bin/gcloud config set $GCSDK_CONFIG_VALUES
        fi
        mkdir -p $GOPATH/bin
        ### install ginkgo
        go get -u github.com/onsi/ginkgo/ginkgo
        ### godotenv
        go get -u github.com/joho/godotenv/cmd/godotenv
        ### install jet
        if [ ! -f /usr/local/bin/jet ]
            then
                cd /tmp
                curl -SsLO $JET_URL
                sudo tar -xaC /usr/local/bin -f $JET_FILE
                sudo chmod +x /usr/local/bin/jet
                rm $JET_FILE
        fi
        ### install migrate
        go get -u github.com/mattes/migrate
        ### install protoc
        if [ ! -f /usr/local/bin/protoc ]
            then         
                curl -SsOL $PROTO_URL
                unzip -qq $PROTO_FILE -d protoc3
                sudo mv protoc3/bin/* /usr/local/bin/
                sudo mv protoc3/include/* /usr/local/include/
                sudo chown $USER /usr/local/bin/protoc
                sudo chown -R $USER /usr/local/include/google
                rm -f $PROTO_FILE
        fi
        ### install stringer
        go get -u golang.org/x/tools/cmd/stringer
        ### install remaining go deps found in makefiles
        ### move this out of pre because we dont want make failing prereqs
        ### we dont need, like minikube
        go get -u github.com/onsi/gomega/...
        go get -u github.com/cespare/reflex
        go get -u github.com/go-swagger/go-swagger/cmd/swagger
        go get -u github.com/tomnomnom/gron
        ### Evans -- GRPC client
        go get github.com/ktr0731/evans
        go get -u github.com/golang/protobuf/protoc-gen-go
        ### glide
        cd $CORE
        if [ ! -d "$CORE/vendor" ]
            then
                curl https://glide.sh/get | sh  # yee har!
                make glide-install
            else
                make glide-install
        fi
        ### build proto things
        go get -u github.com/gogo/protobuf/gogoproto
        cd $GOPATH/src/github.com/gogo/protobuf/gogoproto
        make
        cd $CORE/pkg/timeship/protoc-gen-timeship
        go install
        ln -sf $GOPATH/bin/* $CORE/bin/
        ### Do unholy things to fix timeship
        mkdir -p $CORE/pkg/timeship/googleapis
        ln -sf $CORE/vendor/github.com/googleapis/googleapis/* $CORE/pkg/timeship/googleapis/
        ln -sf $CORE/vendor/github.com $CORE/pkg/timeship/
        mkdir -p $CORE/github.com/gogo/protobuf
        mkdir -p $CORE/gitlab.com/redeam/core/pkg
        mkdir -p $CORE/google/api
        ln -sf $CORE/pkg/timeship $CORE/gitlab.com/redeam/core/pkg/
        ln -sf $CORE/vendor/github.com/gogo/protobuf/gogoproto $CORE/github.com/gogo/protobuf/
        ln -sf $CORE/vendor/github.com/googleapis/googleapis/google/api/* $CORE/google/api/
        ### Get some test things ready
        cp $CORE/config.example.env $CORE/config.env
        ### make regenerate
        cd $CORE
        make regenerate
        ### Bootstrap env vars
        cd $HOME
        export > $HOME/.redeam_env
        grep -q -F 'source .redeam_env' .bash_profile || echo 'source .redeam_env' >> .bash_profile
        grep -q -F 'source .bashrc' .bash_profile || echo 'source .bashrc' >> .bash_profile
        
  SHELL
end
