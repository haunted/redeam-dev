#!/bin/bash
CMD=$1
CMD_ARGS=$2
LAUNCH_PATH=$(pwd)
: ${RDEV_PATH:="/Users/m/Documents/dev/git/redeam-dev"}

echo "redeam-dev host path: $RDEV_PATH"

usage () {
    echo "please provide a command:"
    echo "bootstrap, provision, build, test, syncrepo"
}

build () {
    if [ ! "$1" == "" ]
        then
            build_cmd="cd \$CORE_PATH/$1 && make build"
        else
            build_cmd='cd $CORE_PATH && make build'
    fi
    cd $RDEV_PATH
    vagrant ssh -c "$build_cmd"
}

test () {
    if [ ! "$1" == "" ]
        then
            test_cmd="cd \$CORE_PATH/$1 && make test"
        else
            test_cmd='cd $CORE_PATH && make test'
    fi
    cd $RDEV_PATH
    vagrant ssh -c "$test_cmd"
}

syncrepo () {
    cd $RDEV_PATH
    vagrant rsync-auto
}

bootstrap () {
    cd $RDEV_PATH
    vagrant destroy -f
    vagrant up
}

provision () {
    cd $RDEV_PATH
    vagrant provision
}

if [ "$CMD" == "" ]
    then
        usage
fi

$CMD $CMD_ARGS
