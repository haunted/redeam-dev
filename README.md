# redeam-dev


## Quickstart

1. Install git, VirtualBox, vagrant
2. Clone `core` to your home directory
3. `git clone https://gitlab.com/haunted/redeam-dev.git`
4. `cd redeam-dev && vagrant up  # get a sandwich`
5. `vagrant ssh`
6. `cd $CORE  # Write code, do things`

## VS Code Quickstart (DEPRECATED)

_Note: The VS Code workflow is a little wonky. Shipping the code from your host to the VM (for security) and then editing from the VM is the suggested workflow._

 
1. Install git, VirtualBox, vagrant and VS Code
2. `git clone https://gitlab.com/haunted/redeam-dev.git`
3. set `$RDEV_PATH` to directory of `redeam-dev` in your `.profile`, `.bashrc`, etc
    - **OSX**: set `$RDEV_PATH` in `launchctl` - `echo launchctl setenv RDEV_PATH [your_redeam-dev_path]`
    - **Linux**: set `RDEV_PATH` to be available to your GUI apps (VS Code, etc) as required
4. `git clone https://gitlab.com/redeam/core.git $HOME`
5. `git checkout [your branch] && git pull [your_branch]`
6. `mkdir -p ~/core/.vscode && cp $RDEV_PATH/tasks.json ~/core/.vscode`
7. Launch VS Code and use tasks to manage development
    1. Open the core folder in VS Code Explorer
    2. Select Tasks from the Menu
    3. Select `redeam-bootstrap` task to provision an envionrment
    4. After the task starts, select `redeam-sync-repo` task to initiate rsync watch to core
    5. Edit code
    6. Select `redeam-build-all` to build, `redeam-test-all` to test, or use other tasks to manage dev
8. Profit

_note_: If you switch branches, run `redeam-provision` and restart your `redeam-sync-repo` tasks.

## Summary

This project is a `Vagrantfile` that represents our current `core` development enviornment. 

This project's goal is to provide an OS agnostic, quickstart enviornment for novice and veteran Redeam developers alike. Furthermore, the project serves as a place to concentrate our quality of life efforts from an engineering perspective.

## Requirements

This project leverages _git_, _vagrant_ and _VirtualBox_.

Optionally, should you decide to use VS Code, integration tasks are provided which should allow you to focus on VS Code for the majority of your work.

### git

You'll need `git` on your host to clone this and the core repoistory.

This document assumes the user can install git on their system.

### VirtualBox

Please download VirtualBox and the Extension Pack located below.

[VirtualBox Download Page](https://www.virtualbox.org/wiki/Downloads)

Packages exist for Windows, Linux and OSX.

_NOTE_: This won't work on Windows without tweaking the Vagrantfile. Specifically, the synced folder paths.

### Vagrant

Vagrant is a tool created by Hashicorp that wraps many different hypervisors with a Ruby based DSL.

Please download vagrant located below.
 
[Vagrant Download Page](https://www.vagrantup.com/downloads.html)

Once these tools are installed, you're ready to rock!

## Advanced Usage

#### vim-go

The `redeam-dev` env comes provisioned with `vim-go` and a golang `vimrc`. This provides a powerful toolset for code editing.

To bootstrap, launch `vi` and enter the following commands. `:PlugInstall` followed by `:GoInstallBinaries`.

Once complete, make sure to fully review `:help vim-go` documentation to take advantage of the tools.

#### Evans usage

##### REPL

```
evans --host 127.0.0.1 --port 47462  ./services/account/proto/service.proto
```

#### svclauch usage

```
go build ; POSTGRES_HOST='127.0.0.1' POSTGRES_PORT=65432 ./svclaunch 'console-gw'
```

#### curl usage (console-gw)

```
curl -D- -b cookies -c cookies -X POST 'localhost:39710/api/login' --user 'test:test'
```

#### psql
```
psql -h localhost -p 65432 actourex actourex
```

### Parameters

Note: Not all parms are documented.

The provided `Vagrantfile` is highly parameterized. Environment variables are used to control the VM functionality, and defaults are provided for all values.

Calling `vagrant up` or `vagrant provision` with these set will cause the VM to store these changes.

Defaults are specified in the `Vagrantfile`.

The values are shown in the following format.

The variables on the VM are set without the `RDEV_` prefix.

`VAR` `DEFAULT_VALUE`

`RDEV_VAGRANT_BOX` `minimal/trusty64`  
`RDEV_REDEAM_PATH` `/home/vagrant/go/src/gitlab.com/redeam`  
`RDEV_CORE_PATH` `RDEV_REDEAM_PATH + core`  
`RDEV_CORE_REPO_URL` `https://gitlab.com/redeam/core.git`  
`RDEV_GO_INSTALL_DIR` `/usr/local`  
`RDEV_GO_VERSION` `1.10.2`  
`RDEV_GOPATH` `/home/vagrant/go`  
`RDEV_JET_VERSION` `2.6.0`  
`RDEV_PROTO_VERSION` `3.5.1`  
`RDEV_DOCKER_COMPOSE_VERSION` `1.21.2`  
`RDEV_POSTGRES_SSLMODE` `disabled`  
`RDEV_POSTGRES_DB` `actourex`  
`RDEV_POSTGRES_HOST` `localhost`  
`RDEV_POSTGRES_USER` `actourex`  
`RDEV_POSTGRES_MIGRATIONS` `/pkg/account/backend/postgres/migrations`  
`RDEV_ACCOUNT_ACCESS_TOKEN_TTL` `10m`  
`RDEV_ACCOUNT_LENGTH_TOKEN` `12`  
`RDEV_ACCOUNT_REFRESH_TOKEN_TTL` `60m`  
`RDEV_INFLUXDB_ADDR` `http://influxdb:60086`  
`RDEV_TEST_IN_LOCAL` `true`  
`RDEV_GCS_CREDS_FILE` `$CORE_PATH/.google-account.json`  
`RDEV_GCS_VOUCHER_BUCKET` `training-vouchers`  
`RDEV_GSHEET_CONFIG_FILE` `$CORE_PATH/.google-sheets.json`  
`RDEV_CF_VOUCHER_EXPIRES_IN` `120h`  
`RDEV_CF_USERNAME` `712483d5af8047d0b364bc61076d35f7`  
`RDEV_CF_PASSWORD` `ed02fe1aab3a4daa9f0596d77c74b627`  
`RDEV_CF_LINE_ID` `6N3Rvbg1FW`  
`RDEV_CF_CALLBACK_URL` `https://cf-develop.actourex.io/v1/callback`  
`RDEV_CF_GSHEET_ID` `1UMPQMg6dGanGmiNpkK3MzBDkwE6QCPwUC_AGNOKDIWM`  
`RDEV_SLACK_TOKEN` `foobar`  
`RDEV_PASSHUB_VENDOR_ID` `77221`  
`RDEV_PASSHUB_BASE_URL` `https://app.sightseeingpass.com`  
`RDEV_PASSHUB_PASSWORD` `wehatepaper`  
`RDEV_PASSHUB_USERNAME` `actourex1admin`  

To utilize these variables, either specify the variable prior to your `vagrant up`, or `vagrant provision` or modify `tasks.json` to your specifications.

`RDEV_GO_VERSION=1.9.6 vagrant up`
